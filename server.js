const express = require('express');
const { promisify } = require('util');
const axios = require('axios');
const bodyParser = require('body-parser');
const fs = require('fs');

// .env
require('dotenv').config()

const callUrl = process.env.CALL_URL;

const app = express();
const baseApp = express();

const port = isNaN(parseInt(process.env.PORT, 10)) ? 5000 : parseInt(process.env.PORT, 10);
const baseUrl = process.env.BASE_PATH || '/';

console.log('Base', baseUrl);
console.log('Port', port);

app.use(bodyParser.text({
  type: '*/*'
}));

// Ping api call
app.post("/ping", async function (request, response) {
  const payload = request.body;
  try {
    await axios({
      method: 'post',
      url: callUrl,
      data: {
        value1: payload,
      },
      headers: {'Content-Type': 'application/json'}
    });

    response.status(200).send('OK');
  } catch (e) {
    response.status(500).send('Something went wrong');
  }
  
});

// Static files
app.use(express.static('public'));

app.get("/", async function (request, response) {
  const fileData = await promisify(fs.readFile)(__dirname + '/views/index.html', 'utf-8');
  processedFileData = fileData.replace('{{BASE_URL}}', baseUrl);

  response.status(200).send(processedFileData);
});

baseApp.use(baseUrl, app);

// listen for requests :)
var listener = baseApp.listen(port, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
