$(document).ready(function(){
  console.log('ready');
  
  const showError = function(){
    $("#error").show();
  };
  
  const hideError = function(){
    $("#error").hide();
  };
  
  hideError();
  
  $('#submit').click(function(event) {
    event.preventDefault();
    event.stopPropagation();
    
    const val = $('#name').val();
    
    if (val.trim().length === 0) {
      showError();
    } else {
      hideError();
      $.ajax({
        url: 'ping',
        data: val,
        method: 'POST'
    });
    }
  });
  
});